defmodule Identicon do
  @moduledoc """
  Generate image by name.
  """

  @doc """
  Return grid.

  ## Examples

      iex> Identicon.main("Bob")
      %Identicon.Image{
      color: {47, 193, 192},
      grid: [
      {192, 2},
      {190, 5},
      {146, 7},
      {190, 9},
      {112, 11},
      {150, 12},
      {112, 13},
      {92, 16},
      {254, 17},
      {92, 18},
      {92, 22}
      ],
      hex: [47, 193, 192, 190, 185, 146, 205, 112, 150, 151, 92, 254, 191, 157, 92,
      59],
      pixel_map: [
      {{100, 0}, {150, 50}},
      {{0, 50}, {50, 100}},
      {{100, 50}, {150, 100}},
      {{200, 50}, {250, 100}},
      {{50, 100}, {100, 150}},
      {{100, 100}, {150, 150}},
      {{150, 100}, {200, 150}},
      {{50, 150}, {100, 200}},
      {{100, 150}, {150, 200}},
      {{150, 150}, {200, 200}},
      {{100, 200}, {150, 250}}
      ]
      }

  """
  def main(input) do
    input
    |> hash_input
    |> pick_color
    |> build_grid
    |> filter_odd_squares
    |> build_pixel_map
    |> draw_image
    |> save_image(input)
  end

  def save_image(image, input) do
    File.write("#{input}.png", image)
  end

  def draw_image(%Identicon.Image{color: color, pixel_map: pixel_map}) do
    image = :egd.create(250, 250)
    fill = :egd.color(color)

    Enum.each pixel_map, fn({start,stop}) ->
      :egd.filledRectangle(image, start, stop, fill)
    end

    :egd.render(image)
  end

  def build_pixel_map(%Identicon.Image{grid: grid} = image) do
    pixel_map = Enum.map(grid, fn({_code, index}) ->
      horizontal = rem(index, 5) * 50
      vertical = div(index, 5) * 50

      top_left = {horizontal, vertical}
      bottom_rigth = {horizontal + 50, vertical + 50}

      {top_left, bottom_rigth}
    end)

    %Identicon.Image{image | pixel_map: pixel_map}
  end

  def filter_odd_squares(%Identicon.Image{grid: grid} = image) do
    grid = Enum.filter(grid, fn({code, _index}) ->
      rem(code, 2) == 0
    end)

    %Identicon.Image{image | grid: grid}
  end

  def build_grid(%Identicon.Image{hex: hex} = image) do
    grid = hex
    |> Enum.chunk(3)
    |> Enum.map(&mirrow_row/1)
    |> List.flatten
    |> Enum.with_index

    %Identicon.Image{image | grid: grid}
  end

  def mirrow_row(row) do
    [first, second | _tail] = row
    row ++ [second, first]
  end

  def pick_color(image) do
    %Identicon.Image{hex: [red, green, blue | _tail]} = image

    %Identicon.Image{image | color: {red, green, blue}}
  end

  def hash_input(input) do
    hex = :crypto.hash(:md5, input)
    |> :binary.bin_to_list

    %Identicon.Image{hex: hex}
  end
end
